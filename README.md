**Widentd** is a small [ident/rfc1413](http://www.rfc-editor.org/rfc/rfc1413) deamon that provides a fixed
(and fake) auth reply, regardless of the IP/port pair quoted.


It's intended use is on firewalls, and NAT machines – where you
may want to simply syphon off auth-requests from, for example,
IRC servers.


Widentd is developed, and maintained on a [FreeBSD](https://FreeBSD.org) based system.
But should work equally well on any UNIX-ish system.


Widentd was created, and developed by Dirk-Willem van Gulik <dirkx@webweaving.org>
Widentd is currently developed, and maintained by Chris Hutchinson
<portmaster@BSDforge.com>


Widentd is developed on a [FreeBSD](https://FreeBSD.org) system. As such, I've taylored
widentd.c specifically for this. If you intend to use widentd on
anything other than a [FreeBSD](https://FreeBSD.org) system, you will want to work with
the file widentd.c.other. Simply rename/copy that file to widentd.c
and adjust it for your needs.


For the latest version, and more information, please see:

[BSDforge projects Codeberg](https://codeberg.org/BSDforge/widentd)

* * *
- 1.05	2024-01-27 cleanups/changes/additions (improvements) to source, and docs/man page
- 1.04	2015-02-22 cleanups, revise man(8) page
- 1.03	2015-01-15 light cod cleanup, testing against 11-CURRENT (FreeBSD)
- 1.03	01/2004	IPv6, 64bit safe
- 1.02	12/2003	various timeout changes and warning cleanups
- 1.01	12/2003	memory leak fix.
- 1.00	2001	first release
